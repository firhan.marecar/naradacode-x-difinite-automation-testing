#Author: firhan.marecar@naradacode.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@Login
Feature: Login Success
  Login Scenario Success for SauceDemo Website

  @Positive
  Scenario Outline: Login Success with valid credentials
    Given User on SauceDemo Login Page
    When User input valid <username> with valid <password>
    And User click on login button
    Then User will be directed to homepage

    Examples: 
      | username  | password |
      | standard_user     |     secret_sauce    |  
      
      
        