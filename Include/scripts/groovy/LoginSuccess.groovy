import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class LoginSuccess {

	@Given("User on SauceDemo Login Page")
	def userOnLoginPage() {

		WebUI.openBrowser('')

		WebUI.navigateToUrl('https://www.saucedemo.com/')

		WebUI.maximizeWindow()
	}

	@When("User input valid (.*) with valid (.*)")
	def InputValidCredentials(String username, String password) {

		WebUI.setText(findTestObject('Object Repository/SauceDemo/Login Page/Page_Swag Labs/input_Swag Labs_user-name'), username)

		WebUI.setText(findTestObject('Object Repository/SauceDemo/Login Page/Page_Swag Labs/input_Swag Labs_password'),password)
	}

	@And("User click on login button")
	def loginButton() {

		WebUI.verifyElementText(findTestObject('Object Repository/SauceDemo/Login Page/Page_Swag Labs/div_Swag Labs'), 'Swag Labs')

		WebUI.verifyElementPresent(findTestObject('Object Repository/SauceDemo/Login Page/Page_Swag Labs/input_Swag Labs_login-button'),0)

		WebUI.click(findTestObject('Object Repository/SauceDemo/Login Page/Page_Swag Labs/input_Swag Labs_login-button'))
	}

	@Then("User will be directed to homepage")
	def verifyHomePage() {

		WebUI.verifyElementText(findTestObject('Object Repository/SauceDemo/Login Page/Page_Swag Labs/span_Products'), 'Products')

		WebUI.waitForElementPresent(findTestObject('Object Repository/SauceDemo/Login Page/Page_Swag Labs/span_Products'), 0)

		WebUI.closeBrowser()
	}
}