<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Pokemon Test Suite</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>17a221e6-2052-413f-bb27-4894d180a457</testSuiteGuid>
   <testCaseLink>
      <guid>58f3c365-ec7e-4176-aa9a-85e77b543f58</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Pokemon Test Cases/Pokemon name and type/Pokemon Charizard</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>3e164fd0-f6d9-4b73-8901-e94c0363edcc</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>6b2ecdc5-bd55-43e1-b573-96d8bc27ac5a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Pokemon Test Cases/Pokemon name and type/Pokemon Pikachu</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>908b223c-4da4-4383-b224-a19dbd525b02</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/examples/API Test/Pokemon/TC01 Gyarados</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>73c9602f-e7a2-443a-8224-88f0edc2781a</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>ecc7a4b3-bc9a-4eb0-942b-93e54afcb950</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Pokemon Test Cases/Pokemon name and type/Pokemon Charmeleon with Parsing Data</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
</TestSuiteEntity>
